package main

import (
	"encoding/json"
	// "fmt"
	"flag"
	"os/exec"
	"regexp"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/common/expfmt"
	"github.com/zserge/atomicwriter"

)

var useSudo bool
var smartctlPath string
var fdiskPath string

func check(e error) {
	if e != nil {
		panic(e)
	}
}

// struct for mapping smartctl output
type DeviceSmartctl struct {
	Device map[string]string `json:"device"`
	ModelName string `json:"model_name"`
	SerialNumber string `json:"serial_number"`
	Wwn map[string]int `json:"wwn"`
	SmartStatus map[string]bool `json:"smart_status"`
	PowerOnTime map[string]int `json:"power_on_time"`
	PowerCycleCount int `json:"power_cycle_count"`
	Temperature map[string]int `json:"temperature"`
}

// holds json value for each device
var device_stat DeviceSmartctl

// run smartctl and parse json
func getDeviceSmartctl(device_name string) DeviceSmartctl {

	var cmd_name string
	var cmd_args []string

	if useSudo {
		cmd_name = "sudo"
		cmd_args = append(cmd_args, smartctlPath)
	} else {
		cmd_name = smartctlPath
	}
	cmd_args = append(cmd_args, "-j", "--all", device_name)

	cmd := exec.Command(cmd_name, cmd_args...)
	out, err := cmd.Output()
	check(err)

	smart_status := DeviceSmartctl{}

	err = json.Unmarshal([]byte(out), &smart_status)
	check(err)

	return smart_status
}

// run fdisk -l and return all /dev/sd* devices
func getDeviceList() []string {

	var cmd_name string
	var cmd_args []string
	device_list := []string{}

	if useSudo {
		cmd_name = "sudo"
		cmd_args = append(cmd_args, fdiskPath)
	} else {
		cmd_name = fdiskPath
	}
	cmd_args = append(cmd_args, "-l")
	cmd := exec.Command(cmd_name, cmd_args...)

	re := regexp.MustCompile(`Disk\s(\/dev\/sd\w+)`)
	out, err := cmd.Output()
	check(err)

	matches := re.FindAllSubmatch([]byte(string(out)), -1)

	for _, i := range matches {
		device_list = append(device_list, string(i[1]))
	}

	return device_list
}

func main() {

	var outfile string
	var successVar float64

	flag.StringVar(&outfile, "o", "/usr/local/node_exporter/textfile/smartctl.prom", "prometheus output file")
	flag.StringVar(&smartctlPath, "smartctl-path", "/usr/sbin/smartctl", "path to smartctl")
	flag.StringVar(&fdiskPath, "fdisk-path", "/sbin/fdisk", "path to fdisk exe")
	flag.BoolVar(&useSudo, "sudo", false, "Use sudo with smartctl call")
	flag.Parse()

	// list of device name strings
	device_list := getDeviceList()

	// prometheus registry and common set of labels
	reg := prometheus.NewRegistry()
	gauge_labels := []string{"device", "model", "serial"}

	// Begin Defining metrics

	success_gauge := prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "smartctl_success",
			Help: "1 if successful, 0 if not",
		},
		gauge_labels,
	)
	reg.MustRegister(success_gauge)

	temp_gauge := prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "smartctl_temp_celsius",
			Help: "device temperature",
		},
		gauge_labels,
	)
	reg.MustRegister(temp_gauge)

	power_on_gauge := prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "smartctl_power_on_hours",
			Help: "Power on time",
		},
		gauge_labels,
	)
	reg.MustRegister(power_on_gauge)

	power_cycle_gauge := prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "smartctl_power_cycle_count",
			Help: "Power cycle count",
		},
		gauge_labels,
	)
	reg.MustRegister(power_cycle_gauge)



	// loop through devices, for each one, set a gauge value
	for _, i := range device_list {
		device_stat = getDeviceSmartctl(i)

		// successVar
		if device_stat.SmartStatus["passed"] {
			successVar = 1
		} else {
			successVar = 0
		}
		success_gauge.WithLabelValues(
			device_stat.Device["name"],
			device_stat.ModelName,
			device_stat.SerialNumber,
		).Set(successVar)

		temp_gauge.WithLabelValues(
			device_stat.Device["name"],
			device_stat.ModelName,
			device_stat.SerialNumber,
		).Set(float64(device_stat.Temperature["current"]))

		power_on_gauge.WithLabelValues(
			device_stat.Device["name"],
			device_stat.ModelName,
			device_stat.SerialNumber,
		).Set(float64(device_stat.PowerOnTime["hours"]))

		power_cycle_gauge.WithLabelValues(
			device_stat.Device["name"],
			device_stat.ModelName,
			device_stat.SerialNumber,
		).Set(float64(device_stat.PowerCycleCount))
	}

	// gather and write stats to file
	gatherers := prometheus.Gatherers{reg}
	gathering, err := gatherers.Gather()
	check(err)

	f, err := atomicwriter.NewWriter(outfile)
	check(err)
	defer f.Close()
	for _, mf := range gathering {
		_, err = expfmt.MetricFamilyToText(f, mf)
		check(err)
	}
}
