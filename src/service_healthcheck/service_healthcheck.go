package main

import (
	// "log"
	"bytes"
	"flag"
	"fmt"
	"net/http"
	"os"
	"os/exec"
	"strings"
	"time"
)

var error_count int = 0

func check(e error) {
	if e != nil {
		panic(e)
	}
}

type ServiceCheck struct {
	name    string
	code    int
	message string
}

func check_service_by_name(service_name string) (srvChk ServiceCheck) {

	srvChk.name = service_name

	cmd := exec.Command("systemctl", "check", service_name)

	out, err := cmd.CombinedOutput()
	if err != nil {
		srvChk.code = 1
		error_count++
	} else {
		srvChk.code = 0
	}

	srvChk.message = strings.TrimSuffix(string(out[:]), "\n")

	return
}

func call_healthchecks_io(hc_uuid string, endpoint string, req_body string) {

	var url string

	if len(endpoint) > 0 {
		url = "https://hc-ping.com/" + hc_uuid + "/" + endpoint
	} else {
		url = "https://hc-ping.com/" + hc_uuid
	}

	var client = &http.Client{
		Timeout: 10 * time.Second,
	}

	if len(req_body) > 0 {
		_, err := client.Post(url, "text/plain", bytes.NewBuffer([]byte(req_body)))
		check(err)
	} else {
		_, err := client.Get(url)
		check(err)
	}
}

func main() {

	// healthchecks.io uuid
	var hc_uuid string

	var num_services int

	var be_quiet bool

	var stat_fmt = make(map[int]string)
	stat_fmt[0] = " [OK] "
	stat_fmt[1] = " [ERROR] "
	var hc_req_body string

	flag.StringVar(&hc_uuid, "u", "", "healthchecks.io uuid")
	flag.BoolVar(&be_quiet, "q", false, "be quiet")
	flag.Parse()

	num_services = flag.NArg()
	if num_services == 0 {
		flag.Usage()
		os.Exit(1)
	}

	if len(hc_uuid) > 0 {
		call_healthchecks_io(hc_uuid, "start", "")
	}

	service_checks := make([]ServiceCheck, num_services)

	var this_stat string
	for srv_idx, srv_name := range flag.Args() {
		service_checks[srv_idx] = check_service_by_name(srv_name)

		this_stat = fmt.Sprintf(
			"%s%s%s",
			service_checks[srv_idx].name,
			stat_fmt[service_checks[srv_idx].code],
			service_checks[srv_idx].message,
		)
		if !(be_quiet) {
			fmt.Println(this_stat)
		}
		hc_req_body = hc_req_body + this_stat + "\n"
	}

	if len(hc_uuid) > 0 {
		call_healthchecks_io(hc_uuid, fmt.Sprintf("%d", error_count), "")
	}

	os.Exit(error_count)

}
