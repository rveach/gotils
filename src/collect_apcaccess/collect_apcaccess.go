package main

import (
	"flag"
	"os/exec"
	"strconv"
	"strings"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/common/expfmt"
	"github.com/zserge/atomicwriter"
)

func getMultiplyBy(metric_key string) float64 {
	switch metric_key {
	case "LOADPCT", "BCHARGE", "MBATTCHG":
		return .01
	case "TIMELEFT", "MINTIMEL":
		return 60
	}
	return 1
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func main() {

	var outfile string
	var apcaccess_exe string

	var tmp_metric string
	var tmp_float float64
	var stat_help string
	var m_float_len int = 64

	apcaccess_data := make(map[string]string)

	// map apcaccess fields to metric names
	var metric_mapping = map[string]string{
		"ALARMDEL":  "ups_alarm_del_seconds",
		"BCHARGE":   "ups_battery_charge_ratio",
		"BATTV":     "ups_battery_volts",
		"CUMONBATT": "ups_cum_time_on_batt_seconds",
		"HITRANS":   "ups_hitrans_volts",
		"LINEV":     "ups_line_volts",
		"LOADPCT":   "ups_load_ratio",
		"LOTRANS":   "ups_lotrans_volts",
		"MAXTIME":   "ups_max_time_seconds",
		"MBATTCHG":  "ups_min_battery_charge_ratio",
		"MINTIMEL":  "ups_min_time_left_seconds",
		"NOMBATTV":  "ups_nom_batt_volts",
		"NOMINV":    "ups_nom_in_volts",
		"TIMELEFT":  "ups_time_left_seconds",
		"TONBATT":   "ups_time_on_batt_seconds",
	}

	flag.StringVar(&outfile, "o", "/usr/local/node_exporter/textfile/apcups.prom", "prometheus output file")
	flag.StringVar(&apcaccess_exe, "b", "/sbin/apcaccess", "apcaccess executable")
	flag.Parse()

	// run the command, the split into key/value pairs
	cmd := exec.Command(apcaccess_exe, "-u")
	out, _ := cmd.Output()
	for _, line := range strings.Split(strings.TrimSuffix(string(out), "\n"), "\n") {
		out_line := strings.SplitN(line, ":", 2)
		apcaccess_data[strings.TrimSpace(out_line[0])] = strings.TrimSpace(out_line[1])
	}

	// metrics registry
	reg := prometheus.NewRegistry()

	// add each metric into the registry
	for key, element := range metric_mapping {
		tmp_metric, _ = apcaccess_data[key]
		tmp_float, _ = strconv.ParseFloat(tmp_metric, m_float_len)
		stat_help = key + " reported by apcaccess"

		stat_gauge := prometheus.NewGaugeVec(
			prometheus.GaugeOpts{
				Name: element,
				Help: stat_help,
			},
			[]string{"upsname", "driver", "upsmode", "hostname", "model"},
		)
		reg.MustRegister(stat_gauge)
		stat_gauge.WithLabelValues(apcaccess_data["UPSNAME"], apcaccess_data["DRIVER"], apcaccess_data["UPSMODE"], apcaccess_data["HOSTNAME"], apcaccess_data["MODEL"]).Set(tmp_float * getMultiplyBy(key))
	}

	// gather metrics
	gatherers := prometheus.Gatherers{reg}
	gathering, err := gatherers.Gather()
	check(err)

	// perform an atomic write to the output location
	f, err := atomicwriter.NewWriter(outfile)
	check(err)
	defer f.Close()
	for _, mf := range gathering {
		_, err = expfmt.MetricFamilyToText(f, mf)
		check(err)
	}
}
