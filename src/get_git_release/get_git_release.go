package main

/*
	get-git-release accepts a github groupname/username and a reponame, then returns the latest release tag

	Exit codes:
	0 - everything is fine.  Tag name is returned via stdout.
	1 - incorrect number of cli arguments are passed
	2 - no tag name returned. This could be returned for any number of reasons, including an incorrect repo
		or calling the command for a repo without any releases.
*/

import (
	"encoding/json"
	"flag"
	"fmt"
	"net/http"
	"os"
)

type GithubReleaseInfo struct {
	Tag string `json:"tag_name"`
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func get_github_release(github_group string, repo_name string) (release_info GithubReleaseInfo) {

	url := fmt.Sprintf("https://api.github.com/repos/%s/%s/releases/latest", github_group, repo_name)

	resp, err := http.Get(url)
	check(err)
	defer resp.Body.Close()

	err = json.NewDecoder(resp.Body).Decode(&release_info)
	check(err)

	return
}

func myUsage() {
	fmt.Printf("Usage: %s [OPTIONS] GithubGroup RepoName ...\n", os.Args[0])
	flag.PrintDefaults()
}

func main() {

	flag.Usage = myUsage

	flag.Parse()
	if flag.NArg() != 2 {
		flag.Usage()
		os.Exit(1)
	}

	release_info := get_github_release(flag.Arg(0), flag.Arg(1))

	if len(release_info.Tag) == 0 {
		os.Exit(2)
	}
	fmt.Printf("%s", release_info.Tag)

}
