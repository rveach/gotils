# gotils

system utilities written in go

These scripts are used to help me monitor my home server, and have served as an exercise to learn go better. Suggestions and improvements are welcome.

See the package repo for pre-compiled binary downloads: https://gitlab.com/rveach/gotils/-/packages
The gotils tarfiles include all scripts, but individual script downloads are also available.

## Prometheus Text File Collectors

A number of prometheus textfile collectors have been written to collect system statistics and store them in a directory for node_exporter. These are slower tasks that should not be coupled with the synchronous node_exporter executions.

The metric format is generated using the official prometheus libraries and files are written using an atomic library.

### collect_apcaccess

This utility will run apcaccess and store select metrics. Executable path and export format are noted in the usage below.

```
collect_apcaccess -h
  -b string
        apcaccess executable (default "/sbin/apcaccess")
  -o string
        prometheus output file (default "/usr/local/node_exporter/textfile/apcups.prom")
```

### collect_smartctl

This utility will list physical disks using fdisk, then export select smartctl stats to a prometheus export file, which should be picked up by node_exporter. Defaults are noted in the usage below.

The export path and executable paths can be changed using cli flags. By default, this will not use sudo, but it will if the `-sudo` flag is used.

```
collect_smartctl -h
Usage of ./collect_smartctl:
  -fdisk-path string
        path to fdisk exe (default "/sbin/fdisk")
  -o string
        prometheus output file (default "/usr/local/node_exporter/textfile/smartctl.prom")
  -smartctl-path string
        path to smartctl (default "/usr/sbin/smartctl")
  -sudo
        Use sudo with smartctl call
```

## System Monitors

### service_healthcheck

This utility runs `systemctl check` for named services and will report the status to [Healthchecks.io](https://healthchecks.io/).

Full Usage is not documented on the script, but here is an example:
```
service_healthcheck -q -u your-uuid-here sshd httpd
```
Where:

* -q makes it quiet
* -u your-uuid-here - replace with your healthchecks.io uuid
* then follow up with a space separated list of systemd services

The script will return 0 if all services are good. If not, the return code will equal an integer equal to the number of failed services.

The script will always run without a healthchecks.io uuid. It just won't report to any external sources.
